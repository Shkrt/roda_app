Sequel.migration do
  up do
    alter_table(:books) do
      add_foreign_key :author_id, :authors
      add_index :author_id
    end
  end

  down do
    drop_foreign_key :author_id
    drop_index :author_id, :authors
  end
end

