Sequel.migration do
  up do
    create_table(:reviews) do
      primary_key :id
      foreign_key :user_id, :users
      foreign_key :book_id, :books
      String :text, text: true
      Integer :rate, null: false
      index [:user_id, :book_id], unique: true
    end
  end

  down do
    drop_table(:reviews)
  end
end

