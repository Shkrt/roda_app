Sequel.migration do
  up do
    create_table(:authors) do
      primary_key :id
      String :first_name
      String :last_name
      Date :birthdate
    end
  end

  down do
    drop_table(:authors)
  end
end

