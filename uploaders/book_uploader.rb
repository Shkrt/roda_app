class BookUploader < Shrine
  # frozen_string_literal: true

require 'image_processing/mini_magick'

  plugin :processing
  plugin :versions
  plugin :delete_raw
  plugin :validation_helpers

  Attacher.validate do
    validate_max_size 10 * 1024 * 1024, message: 'is too large (max is 10 MB)'
    validate_mime_type_inclusion %w[image/svg+xml]
  end

  process(:store) do |io, _context|
    original = io.download

    size_large = resize_to_fill(original, 1200, nil)
    size_medium = resize_to_fill(original, 600, nil)
    size_default = resize_to_limit(original, 150, 150)

    { original: io, default: size_default, large: size_large, medium: size_medium }
  end
end
