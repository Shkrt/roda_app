require_relative 'paginatable'

class Author < Sequel::Model
  plugin :json_serializer

  extend Paginatable

  one_to_many :books
end

