module Paginatable
  def paginate(page_no, page_size)
    ds = DB[:books]
    ds.extension(:pagination).paginate(page_no, page_size)
  end
end
