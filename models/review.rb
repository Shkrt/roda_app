class Review < Sequel::Model
  plugin :json_serializer

  many_to_one :book
  many_to_one :users
end

