require_relative '../uploaders/book_uploader'
require_relative 'paginatable'

class Book < Sequel::Model
  plugin :json_serializer
  extend Paginatable
  include BookUploader::Attachment.new(:image)

  many_to_one :author
  one_to_many :reviews

end
