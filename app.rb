require_relative "models"
require "roda"

class App < Roda
  plugin :json, classes: [Array, Hash, Sequel::Model]
  plugin :json_parser
  plugin :all_verbs
  plugin :halt

  route do |r|
    response['Content-Type'] = 'application/json'

    r.on 'authors', Integer do |id|
      r.is 'books' do
        r.get do
          author = Author[id]
          { boooks: author.books.to_json }
        end
      end

      r.get do
        @author = Author[id]
        { author: @author.to_json }
      end
    end

    r.on 'authors' do
      r.get do
        page = r.params[:page] || 1
        {
          authors: Author.paginate(page, 20).map(&:to_json)
        }
      end

      r.post do
        @author = Author.create(author_params(r))
        { author: @author.to_json }
      end
    end

    r.is 'books' do
      r.get do
        page = r.params[:page] || 1
        {
          books: Book.paginate(page, 20).map(&:to_json)
        }
      end

      r.post do
        @book = Book.create(book_params(r))
        { book: @book.to_json }
      end
    end

    r.on 'books', Integer do |book_id|
      @book = Book[book_id]
      r.halt(404) unless @book

      r.is 'review' do
        r.put do
          review = Review.new(review_params(r))
          review.book = @book
          review.save
          { review: review.to_json }
        end
      end

      r.get do
        {
          book: @book.to_json,
          reviews: @book.reviews.to_json
        }
      end

      r.put do
        @book.update(book_params(r))
        { book: @book.to_json }
      end

      r.delete do
        @book.destroy
        response.status = 204
        {}
      end
    end
  end

  private

  def book_params(r)
    { release_year: r.params['release_year'], image: r.params['image'], title: r.params['title'] }
  end

  def author_params(r)
    { birthdate: r.params['birthdate'], first_name: r.params['first_name'], last_name: r.params['last_name'] }
  end

  def review_params(r)
    { user_id: r.params['user_id'], text: r.params['text'], rate: r.params['rate'] }
  end
end


